# React + Vite

What I've used in this project:

- redux toolkit
- redux persist for saving state with page reloading
- tailwind for styling
- vite for project building
- typescript
- gitlab hosting
- dark/light theme
- mobile / web responsive design