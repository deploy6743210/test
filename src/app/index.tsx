import React from 'react';

import TodoListApp from 'pages/todo-list';

import { withProviders } from './providers';

function App(): React.ReactElement {
  return (
    <div className="bg-theme flex-1 w-full h-full min-h-dvh">
      <TodoListApp />
    </div>
  );
}

export default withProviders(App);
