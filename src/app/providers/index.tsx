import compose from "compose-function";

import { withStore } from "./with-store";
import { withPersist } from "./with-persist";

export const withProviders = compose(withStore, withPersist);
