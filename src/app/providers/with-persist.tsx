import { PersistGate } from 'redux-persist/integration/react';
import React from 'react';

import { persistor } from 'shared/store';

export const withPersist =
  (component: () => React.ReactNode) => (): React.ReactNode => (
    <PersistGate loading={undefined} persistor={persistor}>
      {component()}
    </PersistGate>
  );
