import { Provider } from 'react-redux';
import React from 'react';

import { store } from 'shared/store';

export const withStore =
  (component: () => React.ReactNode) => (): React.ReactNode => (
    <Provider store={store}>{component()}</Provider>
  );
