import React from 'react'
import {createRoot} from 'react-dom/client'

import App from './app'
import './index.css'

createRoot(document.querySelector('#root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
