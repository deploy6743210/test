import React, { useEffect, useState } from 'react';

import { ThemeAlias } from 'shared/types/general.model';

function ThemeButton(): React.ReactNode {
  const [currentTheme, setCurrentTheme] = useState<ThemeAlias>('light');

  const setTheme = (theme: ThemeAlias): void =>
    localStorage.setItem('theme', theme);

  const getCurrentTheme = (): ThemeAlias => {
    const userTheme = localStorage.getItem('theme');
    const systemTheme = window.matchMedia(
      '(prefers-color-scheme: dark)',
    ).matches;

    let theme: ThemeAlias = 'light';

    if (userTheme === 'dark') {
      theme = 'dark';
    } else if (userTheme === 'light') {
      theme = 'light';
    } else if (systemTheme) {
      theme = 'dark';
    }
    setCurrentTheme(() => theme);

    return theme;
  };

  const toggleTheme = (): void => {
    if (getCurrentTheme() === 'dark') {
      document.documentElement.classList.remove('dark');
      setTheme('light');
      setCurrentTheme('light');
    } else {
      document.documentElement.classList.add('dark');
      setTheme('dark');
      setCurrentTheme('dark');
    }
  };

  useEffect(() => {
    getCurrentTheme();
  }, []);

  return (
    <div className="flex flex-row justify-between toggle absolute top-3 right-3">
      <label htmlFor="dark-toggle" className="flex items-center cursor-pointer">
        <div className="relative">
          <input
            type="checkbox"
            name="dark-mode"
            id="dark-toggle"
            className="checkbox hidden"
            onClick={toggleTheme}
          />
          <div className="block border-[1px] dark:border-white border-gray-900 w-14 h-8 rounded-full"></div>
          <div className="dot absolute left-1 top-1 dark:bg-white bg-gray-800 w-6 h-6 rounded-full transition dark:left-7"></div>
        </div>
        <div className="ml-2 capitalize dark:text-white text-gray-900 font-medium">
          {currentTheme}
        </div>
      </label>
    </div>
  );
}

export default ThemeButton;
