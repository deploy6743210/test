import React from 'react';

import { useAppSelector } from 'shared/store';
import { todoSelectors } from 'shared/store/todo-slice';

function TodoCounters(): React.ReactNode {
  const completedCount = useAppSelector(todoSelectors.selectCompletedCount);
  const uncompletedCount = useAppSelector(todoSelectors.selectUncompletedCount);

  return (
    <div className="flex justify-between px-4 py-2">
      <span className="text-text_clr">{`Completed: ${completedCount}`}</span>
      <span className="text-text_clr">{`Uncompleted: ${uncompletedCount}`}</span>
    </div>
  );
}

export default TodoCounters;
