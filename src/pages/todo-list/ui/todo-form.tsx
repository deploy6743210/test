import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { addTodo } from 'shared/store/todo-slice';

const TodoForm: React.FC = () => {
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setInputValue(event.target.value);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    if (inputValue.trim()) {
      dispatch(addTodo({ id: Date.now(), text: inputValue, completed: false }));
      setInputValue('');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="flex flex-row mb-4">
        <input
          type="text"
          value={inputValue}
          onChange={handleChange}
          placeholder="Add a new task"
          className="mr-2 py-2 px-4 rounded-md border border-gray-300 focus:outline-none focus:border-blue-500 w-full "
        />
        <button
          type="submit"
          className="py-2 px-4 bg-blue-500 text-white rounded-md ml-2 hover:bg-blue-600 focus:outline-none focus:bg-blue-600 transition duration-200"
        >
          Add
        </button>
      </div>
    </form>
  );
};

export default TodoForm;
