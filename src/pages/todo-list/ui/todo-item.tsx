import { motion } from 'framer-motion';
import { useDispatch } from 'react-redux';
import React from 'react';

import { toggleTodo, deleteTodo } from 'shared/store/todo-slice';
import { Todo } from 'shared/types/todo.model';
import checkedIcon from 'shared/assets/checked.svg';
import deleteIcon from 'shared/assets/delete.svg';

const TodoItem = ({
  todo,
  isLast = false,
}: {
  todo: Todo;
  isLast: boolean;
}): React.ReactElement => {
  const dispatch = useDispatch();

  const handleClick = (): void => {
    dispatch(toggleTodo(todo.id));
  };

  const handleDeleteTodo = (): void => {
    dispatch(deleteTodo(todo.id));
  };

  return (
    <div
      className={`flex items-center justify-between w-full border-gray-600 cursor-pointer  ${
        !isLast && 'border-b'
      }`}
    >
      <motion.li
        className={`flex items-center justify-left py-2 px-2 w-full  ${
          todo.completed ? 'line-through  text-primary' : 'text-text_clr'
        }`}
        whileTap={{ scale: 0.95 }}
        onClick={handleClick}
      >
        <div className="w-6 h-6 mr-5">
          {todo.completed ? <img src={checkedIcon} /> : <></>}
        </div>
        <p className="w-full break-all">{todo.text}</p>
      </motion.li>
      <motion.li
        className={`flex items-center justify-left py-2 px-2   ${
          todo.completed ? 'line-through  text-primary' : 'text-text_clr'
        }`}
        whileTap={{ scale: 0.95 }}
        onClick={handleDeleteTodo}
      >
        <img className="w-6 h-6" src={deleteIcon} />
      </motion.li>
    </div>
  );
};

export default TodoItem;
