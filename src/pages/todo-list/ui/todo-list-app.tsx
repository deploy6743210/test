import React, { useState } from 'react';

import { TodoTabsType } from 'shared/types/todo.model';

import TodoTabs from './todo-tabs';
import TodoForm from './todo-form';
import TodoCounters from './todo-counters';
import TodoList from './todo-list';
import ThemeButton from './theme-button';

const TodoListApp: React.FC = () => {
  const [activeTab, setActiveTab] = useState<TodoTabsType>('all');

  const handleFilterChange = (newTab: TodoTabsType): void => {
    setActiveTab(newTab);
  };

  return (
    <div className="flex justify-center py-2 ">
      <div className="max-w-6xl w-full bg-bg-col shadow-2xl rounded-lg m-4 relative ">
        <ThemeButton />
        <h1 className="text-lg font-semibold text-center text-text_clr pb-4 pt-8">
          Todo List
        </h1>
        <TodoCounters />
        <div className="flex justify-between items-center p-4">
          <TodoTabs
            handleFilterChange={handleFilterChange}
            activeTab={activeTab}
          />
        </div>
        <div className="px-4 pb-4">
          <TodoForm />
          <TodoList activeTab={activeTab} />
        </div>
      </div>
    </div>
  );
};

export default TodoListApp;
