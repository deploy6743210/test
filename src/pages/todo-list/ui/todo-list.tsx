import { AnimatePresence } from 'framer-motion';
import React from 'react';

import { useAppSelector } from 'shared/store';
import { todoSelectors } from 'shared/store/todo-slice';
import { TodoTabsType } from 'shared/types/todo.model';

import TodoItem from './todo-item';

const TodoList = ({
  activeTab,
}: {
  activeTab: TodoTabsType;
}): React.ReactElement => {
  const todos = useAppSelector(todoSelectors.selectTodos);

  const filteredTodos = todos.filter((todo) => {
    if (activeTab === 'completed') return todo.completed;
    if (activeTab === 'current') return !todo.completed;
    return true;
  });

  return (
    <ul>
      <AnimatePresence>
        {filteredTodos.map((todo, index) => (
          <TodoItem
            isLast={index === filteredTodos.length - 1}
            key={todo.id}
            todo={todo}
          />
        ))}
      </AnimatePresence>
    </ul>
  );
};

export default TodoList;
