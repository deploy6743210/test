import React from 'react';

import { TodoTabsType } from '../../../shared/types/todo.model';

type Props = {
  activeTab: TodoTabsType;
  handleFilterChange: (filter: TodoTabsType) => void;
};

type ButtonProps = {
  activeTab: TodoTabsType;
  handleFilterChange: (filter: TodoTabsType) => void;
  tab: TodoTabsType;
  children: React.ReactNode;
};

const TodoTabButton = ({
  activeTab,
  handleFilterChange,
  tab,
  children,
}: ButtonProps): React.ReactElement => (
  <button
    onClick={() => handleFilterChange(tab)}
    className={`py-1 px-3 rounded-md mr-2 text-text_clr capitalize ${
      activeTab === tab && 'bg-blue-500 text-white'
    }`}
  >
    {children}
  </button>
);

const tabs: TodoTabsType[] = ['all', 'current', 'completed'];

export default function TodoTabs({
  activeTab,
  handleFilterChange,
}: Props): React.ReactElement {
  return (
    <div>
      {tabs.map((tab) => (
        <TodoTabButton
          key={tab}
          activeTab={activeTab}
          handleFilterChange={handleFilterChange}
          tab={tab}
        >
          {tab}
        </TodoTabButton>
      ))}
    </div>
  );
}
