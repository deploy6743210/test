import {
  Action,
  Dispatch,
  ThunkAction,
  ThunkDispatch,
  UnknownAction,
  combineReducers,
  configureStore,
} from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import { useDispatch, TypedUseSelectorHook, useSelector } from 'react-redux';

import { TodoState } from 'shared/types/todo.model';

import todoSlice from './todo-slice';

// Persist config
const persistConfig = {
  key: 'root',
  storage,
};
const rootReducer = {
  [todoSlice.name]: todoSlice.reducer,
};
const appReducer = combineReducers(rootReducer);
const persistedReducer = persistReducer(persistConfig, appReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
    }),
});

export const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
export const useAppDispatch = (): ThunkDispatch<
  {
    todos: TodoState;
  },
  undefined,
  UnknownAction
> &
  Dispatch<UnknownAction> => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;
