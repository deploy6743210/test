import { createSlice, PayloadAction, createSelector } from '@reduxjs/toolkit';

import { Todo, TodoState } from '../types/todo.model';

const initialState: TodoState = {
  todos: [],
};

const todoSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<Todo>) => {
      state.todos.push(action.payload);
    },
    toggleTodo: (state, action: PayloadAction<number>) => {
      const todo = state.todos.find(
        (todoItem) => todoItem.id === action.payload,
      );
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    deleteTodo: (state, action: PayloadAction<number>) => {
      state.todos = state.todos.filter((todo) => todo.id !== action.payload);
    },
  },
});

const selectTodos = (state: { todos: TodoState }): Todo[] => state.todos.todos;
const selectCompletedCount = createSelector(
  selectTodos,
  (todos) => todos.filter((todo) => todo.completed).length,
);
const selectUncompletedCount = createSelector(
  selectTodos,
  (todos) => todos.filter((todo) => !todo.completed).length,
);

export const todoSelectors = {
  selectTodos,
  selectCompletedCount,
  selectUncompletedCount,
};
export const { addTodo, toggleTodo, deleteTodo } = todoSlice.actions;
export default todoSlice;
