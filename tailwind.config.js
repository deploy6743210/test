/* eslint-disable id-blacklist */
/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'bg-col': 'rgb(var(--col-bg) / <alpha-value>)',
        text_clr: 'rgb(var(--col-text) / <alpha-value>)',
        primary: 'rgb(var(--col-primary) / <alpha-value>)',
        theme: 'rgb(var(--theme-col) / <alpha-value>)',
      },
    },
  },
  plugins: [],
};
